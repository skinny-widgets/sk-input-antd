
import { SkInputImpl }  from '../../sk-input/src/impl/sk-input-impl.js';

export class AntdSkInput extends SkInputImpl {

    get prefix() {
        return 'antd';
    }

    get suffix() {
        return 'input';
    }

    afterRendered() {
        super.afterRendered();
        this.mountStyles();
    }


}
