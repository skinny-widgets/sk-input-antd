# Skinny Widgets Input for Antd Theme


input element

```
npm i sk-input sk-input-antd --save
```
```html
<sk-config
    theme="antd"
    base-path="/node_modules/sk-core/src"
    theme-path="/node_modules/sk-theme-antd"
></sk-config>
<sk-input id="myInput1" value="foobar"></sk-input>
<script type="module">
    import { SkInput } from './node_modules/sk-input/index.js';

    customElements.define('sk-input', SkInput);

</script>
```

#### attributes

**value** - value syncronized with internal native element

**disabled** - disabled attribute syncronized with internal native element

**list** - datalist attribute for input

#### slots

**default (not specified)** - draws label for input

**label** - draws label for input

```html
<sk-input id="myInput1">
    <span slot="label">Some Label</span>
</sk-input>
 ```

#### template

id: SkInputTpl